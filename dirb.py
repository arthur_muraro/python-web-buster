import sys
import requests
import argparse
import threading

# http://adrenokrome.hd.free.fr:51001/
# http://webscantest.com

def request(url, start, end):
    with open(args.w, encoding='utf8') as f:
        for line, word in enumerate(f):
            if line >= int(start) and line <= int(end):
                response = requests.head(url + word.rstrip(), allow_redirects=True)


                if response.history == '[<Response [301]>]':
                    print(response.history)

                if 200 >= response.status_code <= 299 or 401 >= response.status_code <= 403 or response.status_code == 301:
                    if response.status_code == 200:
                        urls.append(url + word.rstrip() + "/")
                    print(str(response.status_code)+" ==> "+url+word.rstrip())



def argParser():
    parser = argparse.ArgumentParser(usage='dirb.py [-u URL]')

    parser.add_argument('-u', default='http://www.webscantest.com/', help='URL address of the target website')
    parser.add_argument('-w', default='common.txt', help='path to the WordList')
    parser.add_argument('-t', default=4, type=int, help='Number of threads')
    parser.add_argument('-d', default=4, type=int, help='set deepness of the scan')


    return parser

def parseList(wordlist, threadnb):
    file1 = open(wordlist, 'r')
    countline = 0
    for line in file1:
        countline+=1
    file1.close()

    parts = countline // threadnb
    rest = countline % threadnb

    parsedwordlist=[]
    token = -1
    for thread in range (threadnb) :
        start = token + 1

        end = token + parts
        token = end
        
        if thread == threadnb -1:
            end += rest

        parsedwordlist.append(str(start) + ":" + str(end))

    return parsedwordlist

def threadExecutor(url, threadnb, parsedworlist):
    lastThread = None

    for thread in range(threadnb):

        threadwordlist = parsedworlist[thread]
        threadtokens = threadwordlist.split(":")
        lastThread = threading.Thread(target = request, args = (url, threadtokens[0], threadtokens[1]))
        lastThread.start()

    lastThread.join()

urls = []

if __name__ == "__main__":

    args = argParser().parse_args()

    print('\n' + "dirb.py" + '\n')
    print('URL  -> ' + str(args.u))
    print('WORDLIST -> ' + str(args.w))
    print('NUMBER OF THREADS -> ' + str(args.t))
    print('\n' + '--------------------------------------------------------' + '\n')
    
    threadExecutor(args.u, args.t, parseList(args.w, args.t))

    count = 0
    while True:
        count += 1
        if len(urls) == 0 or args.d == count:
            sys.exit(0)
            
        for index, url in enumerate(urls):
            if url[-5:-1] != ".php" or len(url) == 0:
                threadExecutor(url, args.t, parseList(args.w, args.t))
            urls.pop()[index]
